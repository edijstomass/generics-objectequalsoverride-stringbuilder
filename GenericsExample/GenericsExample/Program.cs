﻿using System;

namespace GenericsExample
{
    class Program
    {
        static void Main(string[] args)
        {
            bool isTrue = Calculator.AreEquals<string>("A", "B");

            bool isTrue2 = Calculator.AreEquals<int>(10, 15);

        }
    }


    public class Calculator
    {
        public static bool AreEquals<T> (T value1, T value2)
        {
            return value1.Equals(value2);
        }

    }
}

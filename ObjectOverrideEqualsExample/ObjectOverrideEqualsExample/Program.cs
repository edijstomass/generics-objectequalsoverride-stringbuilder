﻿using System;

namespace ObjectOverrideEqualsExample
{
    class Program
    {
        static void Main(string[] args)
        {

            //Customer C1 = new Customer();
            //C1.name = "Edijs";
            //C1.lastName = "Tomass";

            //Customer C2 = C1;

            //Console.WriteLine(C1.Equals(C2)); // True

            Customer C1 = new Customer();
            C1.name = "Edijs";
            C1.lastName = "Tomass";

            Customer C2 = new Customer();
            C2.name = "Edijs";
            C2.lastName = "Tomass";

            Console.WriteLine(C1.Equals(C2));
            
        }
    }

    class Customer
    {
        public string name { get; set; }
        public string lastName { get; set; }



        public override bool Equals(object obj)
        {
            if (obj == null)
            {
                return false;
            }

            if (!(obj is Customer))  // Lai programma ļautu salīdzināt tikai ar Customer klasi
            {
                return false;
            }

            return this.name == ((Customer)obj).name &&  // cast obj parameter to Customer type
                   this.lastName == ((Customer)obj).lastName;
        }


        // if u override Equals method, its recommended to override GetHashCode method, or u will get warning
        public override int GetHashCode()  
        {
            return this.name.GetHashCode() ^ this.lastName.GetHashCode();
        }

    }



}

﻿using System;
using System.Text;

namespace StringBuilderExample
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder userString = new StringBuilder("My");
            userString.Append(" Name");
            userString.Append(" Is");
            userString.Append(" Eddy");
            Console.WriteLine(userString);

            // string builder is mutable, it overwrites the same value
        }
    }
}
